import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExpensesComponent } from './components/expenses/expenses.component';
import { HomeComponent } from './components/home/home.component';
import { ReportComponent } from './components/report/report.component';
import { StepsComponent } from './components/steps/steps.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'reporting', component: ReportComponent },
  { path: 'expenses', component: ExpensesComponent },
  { path: 'steps', component: StepsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
