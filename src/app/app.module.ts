import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ReportComponent } from './components/report/report.component';
import { StepsComponent } from './components/steps/steps.component';
import { HomeComponent } from './components/home/home.component';
import { ExpensesComponent } from './components/expenses/expenses.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { ReportService } from './services/report.service';
import { HttpClientModule } from '@angular/common/http'; 
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

import { DialogModule } from "primeng/dialog";
import {OverlayPanelModule} from 'primeng/overlaypanel';

import { LoginComponent } from './components/login/login.component';
import { LoginService } from './services/login.service';
import { LoginUpdaterService } from './services/loginUpdater.service';
import { ExpensesService } from './services/expenses.service';
import { ReactiveFormsModule } from '@angular/forms';
//import all angular material modules
import {MaterialImportsModule} from '../material.module';

@NgModule({
  declarations: [
    AppComponent,
    ReportComponent,
    HomeComponent,
    ExpensesComponent,
    HeaderComponent,
    LoginComponent,
    StepsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FlexLayoutModule,
    FormsModule,
    DialogModule,
    OverlayPanelModule,
    ReactiveFormsModule,
    MaterialImportsModule
  ],
  providers: [ReportService, ExpensesService, LoginService, LoginUpdaterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
