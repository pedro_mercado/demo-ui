export interface ModelField {
  name: String;
  ids: any[];
}
