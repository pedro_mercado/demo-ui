import { Role } from "./role";

export interface User {
    id: any
    name: any;
    lastName: any;
    username: any;
    email: any;
    password?: any;
    dob: any;
    roles?:Role[]
 }