import { Component, OnInit } from '@angular/core';
import { LoginUpdaterService } from './services/loginUpdater.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'my-app';
  token: any = null;

  constructor(private loginUpdater: LoginUpdaterService){}

  ngOnInit(){
    this.token = localStorage.getItem("token")
    this.loginUpdater.currentMessenger.subscribe(
      token => {
        if(token != null){
          this.token = token;
        }
      }
    )
  }
}
