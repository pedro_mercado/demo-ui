import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { User } from '../models/user';

@Injectable()
export class ExpensesService {
  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem("token")
  });

  baseurl = '/api/v1/user';

  getUser(): Observable<any> {
    return this.http.get<any>('https://randomuser.me/api/', {
      headers: this.headers,
    });
  }

  saveUser(user: User){
    return this.http.post(this.baseurl + '/save', user, {headers: this.headers, responseType: 'text',});
  }

}
