import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { User } from '../models/user';

@Injectable()
export class StepsService {
  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem("token")
  });

  baseurl = '/api/v1/user';

  getUsers(offset: number, limit: number, sortBy: string, sortType: string, globalFilter: any, user: User): Observable<any> {
    return this.http.post<any>(this.baseurl + '/' + offset + '/' + limit + '/' + sortBy + '/' + sortType + '?globalFilter=' + globalFilter, user, {
      headers: this.headers,
      observe: 'response',
    });
  }

  exportUsers(sortBy: string, sortType: string, columns:any[], globalFilter: string, user: User){
    return this.http.post(this.baseurl + '/export' + '/' + sortBy + '/' + sortType + '?globalFilter=' + globalFilter + '&columns='+columns, user, {headers: this.headers, responseType: 'text',});
  }

  getModels(){
    return this.http.get('/api/v1/model', {
      headers: this.headers,
      observe: 'response',
    });
  }

  getModelFields(ids: any []){
    return this.http.get('/api/v1/model/' + ids, {
      headers: this.headers,
      observe: 'response',
    });
  }

}
