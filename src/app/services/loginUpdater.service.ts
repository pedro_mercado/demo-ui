import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class LoginUpdaterService {

  private source = new BehaviorSubject(null);
  currentMessenger = this.source.asObservable();

  constructor() { }

  updateToken(token: any) {
    this.source.next(token)
  }

}
