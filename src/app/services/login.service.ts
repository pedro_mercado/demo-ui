import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class LoginService {
  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
  });
  
  baseurl = '/api/v1/user';

  login(authenticationRequest:any): Observable<any> {
    return this.http.post<any>(this.baseurl + '/login', authenticationRequest, {
      headers: this.headers
    });
  }

  getUserDetails(username:string, token: string): Observable<any> {
    return this.http.get<any>(this.baseurl + '/email' + "/" + username,{
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    });
  }
}
