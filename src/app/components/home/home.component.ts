import { Component, OnInit, OnDestroy, ViewChild} from '@angular/core';

@Component({
    selector: 'home-component',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
     providers: []
  })
  export class HomeComponent implements OnInit, OnDestroy {

    constructor(){
        
    }

    ngOnInit(){

    }

    ngOnDestroy(){

    }
  }