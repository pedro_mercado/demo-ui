import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { User } from 'src/app/models/user';
import { StepsService } from 'src/app/services/steps.service';
import { FormGroup, FormBuilder, ValidatorFn, FormControl, FormArray, AbstractControl } from '@angular/forms'; 
import { ModelField } from 'src/app/models/modelField';

@Component({
  selector: 'steps-component',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.css'],
  providers: [StepsService],
})
export class StepsComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort, { static: false }) sort!: MatSort;
  isLinear = true;
  token:any = null;
  isLoading = false;
  sortBy = 'id';
  sortType = 'asc';
  totalRows = 0;
  pageSize = 5;
  currentPage = 0;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  users: any = [];
  userFilter: User = {
    id: null,
    name: '',
    lastName: '',
    username: '',
    email: '',
    dob: null,
  };
  userFilterParam: User = {
    id: null,
    name: '',
    lastName: '',
    username: '',
    email: '',
    dob: null,
  };
  displayedColumns = [];
  formModel!: FormGroup;
  formField!: FormGroup;
  selectedModelIds: any = [];
  modelList: any;
  modelFieldList: any = [];
  controlName:any = ''; 
  modelField!: ModelField;
  selectedModelFieldIds: ModelField[] = [];

  tableColumns: any = [
    { id: 1, column: 'id', label: 'Id', show: true },
    { id: 2, column: 'name', label: 'First Name', show: true },
    { id: 3, column: 'lastName', label: 'Last Name', show: true },
    { id: 4, column: 'username', label: 'User Name', show: true },
    { id: 5, column: 'email', label: 'Email', show: true },
    { id: 6, column: 'dob', label: 'DOB', show: true },
    { id: 7, column: 'role', label: 'Role', show: true },
  ];

  dataSource = new MatTableDataSource<User>();
  globalFilter = '';
  globalFilterParam = '';
  showFilters: boolean = false;
  showColumns: boolean = false;
  step = 0;

  constructor(private stepsService: StepsService, private formBuilder: FormBuilder) {
    this.getModels();
    this.formModel = this.formBuilder.group({
      models: new FormArray([], this.minSelectedCheckboxes(1))
    });
    this.formField = this.formBuilder.group({
    });
  }

  ngOnInit() {
    this.changeColumns();
    this.dataSource.sort = this.sort;
    this.pageable();
  }

  ngOnDestroy() {}

  getModels(){
    this.stepsService.getModels()
    .subscribe((data) => {
      this.modelList = data.body;
      this.modelList.forEach(() => this.modelsFormArray.push(new FormControl(false)));
    },
    err => {
      var error = err;
      if(error != null){
        alert("Error!");
      }
    });
  }

  getModelFields(){
    this.stepsService.getModelFields(this.selectedModelIds)
    .subscribe((data) => {
      this.modelFieldList = data.body;
      //console.log(this.modelFieldList);
      this.modelFieldList.forEach((f:any) => {
        //console.log("for----");
        //console.log(f);
        this.formField.addControl(f.name, new FormArray([], this.minSelectedCheckboxes(1)))        
        f.fields.forEach((a:any) => {
          this.controlName = f.name;
          if(this.fieldsFormArray.controls.length < f.fields.length){
            this.fieldsFormArray.push(new FormControl(false))
            //console.log("nested for----");
            //console.log(a);
          }
        });
      });
      console.log(this.formField.controls);
    },
    err => {
      var error = err;
      if(error != null){
        alert("Error!");
      }
    });
  }

  get modelsFormArray() {
    return this.formModel.controls.models as FormArray;
  }

  get fieldsFormArray() {
    return this.formField.get(this.controlName) as FormArray;
  }

  checkValues(model:any) {
    //console.log(model);
    //console.log(this.formField.controls);   
    this.step = 0;  
    this.formField.removeControl(model);
    //console.log(this.formField.controls);
    this.selectedModelIds = this.formModel.value.models
      .map((v:any, i:any) => v ? this.modelList[i].id : null)
      .filter((v:any) => v !== null);
    //console.log(this.selectedModelIds);
    //console.log(this.formModel.value.models);
    
  }

  checkFieldValues() {
    this.selectedModelFieldIds = [];
    //console.log('print selected Models: ')
    //console.log(this.modelFieldList);
    for(let i = 0; i < this.modelFieldList.length; i++){
      //console.log('createm modelField ' + this.modelFieldList[i].name +' instance: ');
      this.modelField = {name: this.modelFieldList[i].name, ids: []};
      //console.log(this.modelField);
      for(let j = 0; j < this.modelFieldList[i].fields.length; j++ ){
        //console.log('print formField: ' + this.modelFieldList[i].fields[j].name + ' : '+ this.formField.get(this.modelFieldList[i].name)?.value[j]);
        if(this.formField.get(this.modelFieldList[i].name)?.value[j]){
        //console.log(this.modelFieldList[i].fields[j].id + ' has been selected');
          this.modelField.ids.push({'id': this.modelFieldList[i].fields[j].id, 'name': this.modelFieldList[i].fields[j].name});
        }
        if(this.selectedModelFieldIds.findIndex(field => field.name === this.modelFieldList[i].name) == -1){
          this.selectedModelFieldIds.push(this.modelField);
        }
      }
      //console.log('**Final list**');
      //console.log(this.selectedModelFieldIds);
      //console.log('----modelField final----');
      //console.log(this.modelField);
    }
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: AbstractControl) => {
      if (formArray instanceof FormArray) {
        const totalSelected = formArray.controls
          .map((control) => control.value)
          .reduce((prev, next) => (next ? prev + next : prev), 0);
        return totalSelected >= min ? null : { required: true };
      }
  
      throw new Error('formArray is not an instance of FormArray');
    };
  
    return validator;
  }

  nextAction(){
    //console.log("Next Step");
    this.getModelFields();
  }

  selectedFields(){
    console.log(this.selectedModelFieldIds);
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  pageChanged(event: any) {
    console.log(event);
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.pageable();
  }

  pageable() {
    this.stepsService
      .getUsers(
        this.currentPage * this.pageSize,
        this.pageSize,
        this.sortBy,
        this.sortType,
        this.globalFilterParam,
        this.userFilterParam
      )
      .subscribe((data) => {
        this.totalRows = Number(data.headers.get('X-Total-Entries'));
        this.users = data.body;
        this.dataSource.data = data.body;
        //console.log(this.users[0].roles);
      });
  }

  sortEvent(event: any) {
    console.log(event);
    if (event.direction != '') {
      this.sortBy = event.active;
      this.sortType = event.direction;
      this.pageable();
    }
  }

  filter() {
    this.showFilters = true;
  }

  filtering(){
    this.globalFilterParam = "";
    this.globalFilter = "";
    this.userFilterParam = this.userFilter;
    this.pageable();
    this.showFilters = false;
  }

  globalFiltering() {
    this.globalFilterParam = this.globalFilter;
    this.initializeUserFilter();
    this.pageable();
  }

  showColumnPopup() {
    this.showColumns = true;
  }

  changeColumns() {
    this.displayedColumns = this.tableColumns
      .filter((col: any) => col.show)
      .map((col: any) => col.column);
  }

  initializeUserFilter() {
    this.userFilter = {
      id: null,
      name: '',
      lastName: '',
      username: '',
      email: '',
      dob: null,
    };
    this.userFilterParam = this.userFilter;
  }

  exportUsers() {
    let filename = 'userReport.csv';
    this.stepsService
      .exportUsers(
        this.sortBy,
        this.sortType,
        this.displayedColumns,
        this.globalFilterParam,
        this.userFilterParam
      )
      .subscribe(
        (data) => {
          let binaryData = [];
          binaryData.push(data);
          let downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(
            new Blob(binaryData, { type: 'text' })
          );
          if (filename) downloadLink.setAttribute('download', filename);
          document.body.appendChild(downloadLink);
          downloadLink.click();
          document.body.removeChild(downloadLink);
        },
        (err) => {
          var errorMessage = err;
          if (errorMessage != null) {
            alert('Error, Please try again.');
          }
        }
      );
  }
}
