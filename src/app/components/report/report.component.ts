import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ReportService } from 'src/app/services/report.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { User } from 'src/app/models/user';
import { Role } from 'src/app/models/role';

@Component({
  selector: 'report-component',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css'],
  providers: [ReportService],
})
export class ReportComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort, { static: false }) sort!: MatSort;
  token:any = null;
  isLoading = false;
  sortBy = 'id';
  sortType = 'asc';
  totalRows = 0;
  pageSize = 5;
  currentPage = 0;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  users: any = [];
  userFilter: User = {
    id: null,
    name: '',
    lastName: '',
    username: '',
    email: '',
    dob: null,
  };
  userFilterParam: User = {
    id: null,
    name: '',
    lastName: '',
    username: '',
    email: '',
    dob: null,
  };
  displayedColumns = [];

  tableColumns: any = [
    { id: 1, column: 'id', label: 'Id', show: true },
    { id: 2, column: 'name', label: 'First Name', show: true },
    { id: 3, column: 'lastName', label: 'Last Name', show: true },
    { id: 4, column: 'username', label: 'User Name', show: true },
    { id: 5, column: 'email', label: 'Email', show: true },
    { id: 6, column: 'dob', label: 'DOB', show: true },
    { id: 7, column: 'role', label: 'Role', show: true },
  ];

  dataSource = new MatTableDataSource<User>();
  globalFilter = '';
  globalFilterParam = '';
  showFilters: boolean = false;
  showColumns: boolean = false;

  constructor(private reportService: ReportService) {}

  ngOnInit() {
    this.token = localStorage.getItem("token")
    this.changeColumns();
    this.dataSource.sort = this.sort;
    this.pageable();
  }

  ngOnDestroy() {}

  pageChanged(event: any) {
    console.log(event);
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.pageable();
  }

  pageable() {
    this.reportService
      .getUsers(
        this.currentPage * this.pageSize,
        this.pageSize,
        this.sortBy,
        this.sortType,
        this.globalFilterParam,
        this.userFilterParam
      )
      .subscribe((data) => {
        this.totalRows = Number(data.headers.get('X-Total-Entries'));
        this.users = data.body;
        this.dataSource.data = data.body;
        //console.log(this.users[0].roles);
      });
  }

  sortEvent(event: any) {
    console.log(event);
    if (event.direction != '') {
      this.sortBy = event.active;
      this.sortType = event.direction;
      this.pageable();
    }
  }

  filter() {
    this.showFilters = true;
  }

  filtering(){
    this.globalFilterParam = "";
    this.globalFilter = "";
    this.userFilterParam = this.userFilter;
    this.pageable();
    this.showFilters = false;
  }

  globalFiltering() {
    this.globalFilterParam = this.globalFilter;
    this.initializeUserFilter();
    this.pageable();
  }

  showColumnPopup() {
    this.showColumns = true;
  }

  changeColumns() {
    this.displayedColumns = this.tableColumns
      .filter((col: any) => col.show)
      .map((col: any) => col.column);
  }

  initializeUserFilter() {
    this.userFilter = {
      id: null,
      name: '',
      lastName: '',
      username: '',
      email: '',
      dob: null,
    };
    this.userFilterParam = this.userFilter;
  }

  exportUsers() {
    let filename = 'userReport.csv';
    this.reportService
      .exportUsers(
        this.sortBy,
        this.sortType,
        this.displayedColumns,
        this.globalFilterParam,
        this.userFilterParam
      )
      .subscribe(
        (data) => {
          let binaryData = [];
          binaryData.push(data);
          let downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(
            new Blob(binaryData, { type: 'text' })
          );
          if (filename) downloadLink.setAttribute('download', filename);
          document.body.appendChild(downloadLink);
          downloadLink.click();
          document.body.removeChild(downloadLink);
        },
        (err) => {
          var errorMessage = err;
          if (errorMessage != null) {
            alert('Error, Please try again.');
          }
        }
      );
  }
}
