import { Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import { Role } from 'src/app/models/role';
import { User } from 'src/app/models/user';
import { LoginUpdaterService } from 'src/app/services/loginUpdater.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
     providers: []
  })
  export class HeaderComponent implements OnInit, OnDestroy {
    user: User = {
      id: null,
      name: '',
      lastName: '',
      username: '',
      email: '',
      dob: null,
    };

    constructor(private router?: Router, private loginUpdater?: LoginUpdaterService){}

    ngOnInit(){
      this.user = JSON.parse(localStorage.getItem('loggedUser') || '{}')[0];
      this.user.roles![0].role = this.user.roles![0].role.toLowerCase(); 
      
      if (this.tokenExpired(localStorage.getItem("token"))) {
        this.logout();
      } 
    }

    ngOnDestroy(){

    }

    private tokenExpired(token: any) {
      const expiry = (JSON.parse(atob(token.split('.')[1]))).exp;
      return (Math.floor((new Date).getTime() / 1000)) >= expiry;
    }

    logout(){
      localStorage.removeItem("token");
      this.loginUpdater!.updateToken('');
      this.router!.navigate(['/']);
      this.user = {
        id: null,
        name: '',
        lastName: '',
        username: '',
        email: '',
        dob: null
      };
    }
  }