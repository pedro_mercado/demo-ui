import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { zip } from 'rxjs';
import { Role } from 'src/app/models/role';
import { User } from 'src/app/models/user';
import { ExpensesService } from 'src/app/services/expenses.service';

@Component({
  selector: 'expenses-component',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css'],
  providers: [ExpensesService],
})
export class ExpensesComponent implements OnInit, OnDestroy {
  roles: any = [{ id: 1, role: 'USER' }];
  user: User = {
    id: 0,
    name: '',
    lastName: '',
    username: '',
    email: '',
    password: 'temp',
    dob: '',
    roles: this.roles,
  };

  constructor(private expensesService: ExpensesService) {}

  ngOnInit() {}

  ngOnDestroy() {}

  saveNewUser() {
    this.expensesService.getUser().subscribe((data) => {
      console.log(data.results[0].name.first + " " + data.results[0].name.last);
      if(this.isValid(data.results[0].name.first + data.results[0].name.last)){
        this.user.name = data.results[0].name.first;
        this.user.lastName = data.results[0].name.last;
        this.user.username = data.results[0].login.username;
        this.user.email = data.results[0].email;
        this.user.dob = data.results[0].dob.date;
        console.log(this.user);
        this.saveUserRequest(this.user);
      }
    });
  }

  isValid(str: string){
    let valid = true;
    for(let i = 0; i < str.length; i++){
      let code = str.charCodeAt(i);
      if (!(code > 64 && code < 91) && !(code > 96 && code < 123)) {
        console.log(code);
        console.log(valid);
        valid = false;
      }
    }
    return valid;
  }

  saveUserRequest(user: User) {
    this.expensesService.saveUser(user).subscribe((data) => {
      console.log(data);
    });
  }
}
