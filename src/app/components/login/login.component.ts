import { Component, OnInit, OnDestroy} from '@angular/core';
import { LoginUpdaterService } from 'src/app/services/loginUpdater.service';
import { LoginService } from 'src/app/services/login.service';
import jwt_decode from "jwt-decode";
import { User } from 'src/app/models/user';
import { HeaderComponent } from '../shared/header/header.component';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'; 

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
     providers: [LoginService]
  })
  export class LoginComponent implements OnInit, OnDestroy {
    myForm!: FormGroup;
    //authenticationRequest: any = {"userName": "", "password": ""}
    errorMessage: boolean = false;
    token: any;
    user: User = {
      id: null,
      name: '',
      lastName: '',
      username: '',
      email: '',
      dob: null,
    };

    constructor(private loginService: LoginService, private loginUpdater: LoginUpdaterService, private fb: FormBuilder){
        
    }

    ngOnInit(){
      this.myForm = this.fb.group({
        userName: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(4)]]
      });
    }

    login(){
      console.log('Email', this.myForm.value.userName);
      console.log('Email', this.myForm.value.password);

      this.loginService.login(this.myForm.value)
      .subscribe((data) => {
        localStorage.setItem("token", data.jwt);
        this.token = data.jwt;
        this.loginUpdater.updateToken(data.jwt);
        let decoded: any = jwt_decode(this.token);

        this.getUserDetails(decoded.sub, data.jwt);
      },
      err => {
        var error = err;
        if(error != null){
          this.errorMessage = true;
        }
      });
    }

    getUserDetails(username: string, token: string){
      this.loginService.getUserDetails(username, token).subscribe((data) => {
        this.user = data;
        localStorage.setItem('loggedUser', JSON.stringify(this.user));
        let headerComponent = new HeaderComponent();
        headerComponent.ngOnInit();
      })
    }

    ngOnDestroy(){

    }
  }